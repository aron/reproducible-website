---
layout: post
title: "Supporter spotlight: Google Open Source Security Team (GOSST)"
date: 2022-04-26 10:00:00
categories: org
---

[![]({{ "/images/news/supporter-spotlight-gosst/gosst.png?2#right" | relative_url }})](https://security.googleblog.com/)

<big>The Reproducible Builds project relies on [several projects, supporters
and sponsors]({{ "/who/" | relative_url }}) for financial support, but they are
also valued as ambassadors who spread the word about our project and the work
that we do.</big>

This is the fourth instalment in a series featuring the projects, companies
and individuals who support the Reproducible Builds project. If you are a
supporter of the Reproducible Builds project (of whatever size) and would like
to be featured here, please let get in touch with us at
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).

We started this series by
[featuring the Civil Infrastructure Platform]({{ "/news/2020/10/21/supporter-spotlight-cip-project/" | relative_url }})
project and followed this up with a
[post about the Ford Foundation]({{ "/news/2021/04/06/supporter-spotlight-ford-foundation/" | relative_url }})
as well as a [recent one about ARDC]({{ "/news/2022/04/14/supporter-spotlight-ardc/" | relative_url }}). However,
today, we'll be talking with **Meder Kydyraliev** of the
[**Google Open Source Security Team** (GOSST)](https://security.googleblog.com/).

<br>

**Chris Lamb: Hi Meder, thanks for taking the time to talk to us today. So, for
someone who has not heard of the Google Open Source Security Team (GOSST)
before, could you tell us what your team is about?**

Meder: Of course. The Google Open Source Security Team (or 'GOSST') was created
in 2020 to work with the open source community at large, with the goal of
making the open source software that everyone relies on more secure.

<br>

**Chris: What kinds of activities is the GOSST involved in?**

Meder: The range of initiatives that the team is involved in recognizes the
diversity of the ecosystem and unique challenges that projects face on their
security journey. For example, our sponsorship of [*sos.dev*](http://sos.dev/)
ensures that developers are rewarded for security improvements to open source
projects, whilst the long term work on improving
[Linux kernel security](http://sec.org/wiki/index.php/Kernel_Self_Protection_Project)
tackles specific kernel-related vulnerability classes.

[![]({{ "/images/news/supporter-spotlight-gosst/sigstore.png#right" | relative_url }})](https://sigstore.dev/)

Many of the projects GOSST is involved with aim to make it easier for
developers to improve security through automated assessment
([Scorecards](https://github.com/ossf/scorecard) and
[Allstar](https://github.com/ossf/allstar)) and vulnerability discovery tools
([OSS-Fuzz](https://google.github.io/oss-fuzz/), [ClusterFuzzLite](https://github.com/google/clusterfuzzlite),
[FuzzIntrospector](https://github.com/ossf/fuzz-introspector)), in addition to
contributing to infrastructure to make adopting certain ‘best practices’
easier. Two great examples of best practice efforts are
[Sigstore](https://sigstore.dev/) for artifact signing and
[OSV](https://osv.dev/) for automated vulnerability management.

<br>

**Chris: The usage of open source software has exploded in the last decade, but
supply-chain hygiene and best practices has seemingly not kept up. How does
GOSST see this issue and what approaches is it taking to ensure that past and
future systems are resilient?**

Meder: Every open source ecosystem is a complex environment and that awareness
informs our approaches in this space. There are, of course, no ‘silver
bullets’, and long-lasting and material supply-chain improvements requires
infrastructure and tools that will make lives of open source developers easier,
all whilst improving the state of the wider software supply chain.

As part of a broader effort, we created the [Supply-chain Levels for Software Artifacts](https://slsa.dev/)
framework that has been used internally at Google to protect
production workloads. This framework describes the best practices for source
code and binary artifact integrity, and we are engaging with the community on
its refinement and adoption. Here, package managers (such as PyPI, Maven
Central, Debian, etc.) are an essential link in the software supply chain due
to their near-universal adoption; users do not download and compile their own
software anymore.  GOSST is starting to work with package managers to explore
ways to collaborate together on  improving the state of the supply chain and
helping package maintainers and application developers do better… all with the
understanding that many open source projects are developed in spare time as a
hobby!
[Solutions like this](https://security.googleblog.com/2022/04/improving-software-supply-chain.html),
which are the result of [collaboration between GOSST and GitHub](https://github.blog/2022-04-07-slsa-3-compliance-with-github-actions/),
are very encouraging as they demonstrate a way to materially strengthen
software supply chain security with readily available tools, while also
improving development workflows.

For GOSST, the problem of supply chain security also covers vulnerability
management and solutions to make it easier for everyone to discover known
vulnerabilities in open source packages in a scalable and automated way. This
has been difficult in the past due to lack of consistently
high-quality data in an easily-consumable format. To address this, we’re working
on infrastructure ([OSV.dev](https://osv.dev/)) to make vulnerability data more easily accessible
as well as a widely adopted and automation friendly [data format](https://ossf.github.io/osv-schema).

<br>

**Chris: How does the Reproducible Builds effort help GOSST achieve its goals?**

Meder: Build reproducibility has a lot of attributes that are attractive as
part of generally good ‘build hygiene’. As an example, hermeticity, one of
[requirements to meet SLSA level 4](https://slsa.dev/spec/v0.1/levels#summary-of-levels),
makes it much easier to reason about the dependencies of a piece of software.
This is an enormous benefit during vulnerability or supply chain incident
response.

On a higher level, however, we always think about reproducibility from the
viewpoint of a user and the threats that reproducibility protects them from.
Here, a lot of progress has been made, of course, but a lot of work remains to
make reproducibility part of everyone's software consumption practices.

<br>

[![]({{ "/images/news/supporter-spotlight-gosst/gosst.png#right" | relative_url }})](https://security.googleblog.com/)

**Chris: So if someone wanted to know more about GOSST or follow the team's
work, where might they go to look?**

Meder: We post regular updates on [Google's Security Blog](https://security.googleblog.com/)
and on the [Linux hardening mailing list](https://lore.kernel.org/linux-hardening/).
We also welcome community participation in the projects we work on! See any of
the projects linked above or [OpenSSF's GitHub projects page](https://github.com/ossf)
for a list of efforts you can contribute to directly if you want to get
involved in strengthening the open source ecosystem.

<br>

**Chris: Thanks for taking the time to talk to us today.**

Meder: No problem. :)

<br>

---

<br>

*For more information about the Reproducible Builds project, please see our website at
[reproducible-builds.org]({{ "/" | relative_url }}). If you are interested in
ensuring the ongoing security of the software that underpins our civilisation
and wish to sponsor the Reproducible Builds project, please reach out to the
project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).*
